# Order Processing System Deployment

Deployment Script of order processing system

## Getting Started

These instructions will guild you to start all the microservices in your local machine for development and testing purposes.

### Prerequisites

Docker 

### Installing

Pull latest changes of your projects

Run the build.sh in each project directory. This is a one time task which you don't have to do unless you have a change in your project

```
./build.sh
```

Navigate your console to the deployment folder which has the docker-compose.yml. Run following

```
docker-compose up
```


This will start all your applications in respective ports